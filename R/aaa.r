httr::user_agent(
  sprintf(
    "mediacloud package v%s: (<%s>)",
    utils::packageVersion("mediacloud"),
    utils::packageDescription("mediacloud")$URL
  )
) -> .MEDIACLOUD_UA
