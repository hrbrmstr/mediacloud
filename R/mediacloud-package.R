#' Tools to Query the 'Media Cloud' 'API'
#'
#' 'Media Cloud' (<https://mediacloud.org/>) is an open source platform
#' for studying media ecosystems. By tracking millions of stories published online,
#' their suite of tools allows researchers to track how stories and ideas spread
#' through media and help researchers answer complex quantitative and qualitative
#' questions about the content of online media. Methods are provided to query
#' the 'Media Cloud' 'API'.
#'
#' @md
#' @name mediacloud
#' @keywords internal
#' @author Bob Rudis (bob@@rud.is)
#' @import httr
#' @import stringi
#' @importFrom scales comma
#' @importFrom jsonlite fromJSON
"_PACKAGE"
