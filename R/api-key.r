#' Get or set MEDIACLOUD_API_KEY value
#'
#' The API wrapper functions in this package all rely on a Media Cloud API
#' key residing in the environment variable \code{MEDIACLOUD_API_KEY}. The
#' easiest way to accomplish this is to set it in the `\code{.Renviron}` file in your
#' home directory.
#'
#' Every API call must include a key parameter which will authenticate you to
#' the API service. To get a key, register for a user via
#' <https://topics.mediacloud.org/#/user/signup>. Once you have an account go
#' here to see your key <https://topics.mediacloud.org/#/user/profile>.
#'
#' @param force force setting a new Media Cloud API key for the current environment?
#' @return atomic character vector containing the Media Cloud API key
#' @references <https://github.com/berkmancenter/mediacloud/blob/master/doc/api_2_0_spec/api_2_0_spec.md>
#' @export
mediacloud_api_key <- function(force = FALSE) {

  env <- Sys.getenv('MEDIACLOUD_API_KEY')
  if (!identical(env, "") && !force) return(env)

  env <- Sys.getenv("MEDIACLOUD_API_KEY")
  if (!identical(env, "") && !force) {
    message("MEDIACLOUD_API_KEY is deprecated, please update environment variable to MEDIACLOUD_API_KEY")
    return(env)
  }

  if (!interactive()) {
    stop("Please set env var MEDIACLOUD_API_KEY to your Media Cloud API key",
      call. = FALSE)
  }

  message("Couldn't find env var MEDIACLOUD_API_KEY See ?MEDIACLOUD_API_KEY for more details.")
  message("Please enter your API key and press enter:")
  pat <- readline(": ")

  if (identical(pat, "")) {
    stop("Media Cloud API key entry failed", call. = FALSE)
  }

  message("Updating MEDIACLOUD_API_KEY env var to PAT")
  Sys.setenv(MEDIACLOUD_API_KEY = pat)

  pat

}
