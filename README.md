
[![Travis-CI Build
Status](https://travis-ci.org/hrbrmstr/mediacloud.svg?branch=master)](https://travis-ci.org/hrbrmstr/mediacloud)
[![Coverage
Status](https://codecov.io/gh/hrbrmstr/mediacloud/branch/master/graph/badge.svg)](https://codecov.io/gh/hrbrmstr/mediacloud)
[![CRAN\_Status\_Badge](https://www.r-pkg.org/badges/version/mediacloud)](https://cran.r-project.org/package=mediacloud)

# mediacloud \[WIP\]

Tools to Query the ‘Media Cloud’ ‘API’

## Description

‘Media Cloud’ (<https://mediacloud.org/>) is an open source platform for
studying media ecosystems. By tracking millions of stories published
online, their suite of tools allows researchers to track how stories and
ideas spread through media and help researchers answer complex
quantitative and qualitative questions about the content of online
media. Methods are provided to query the ‘Media Cloud’ ‘API’.

## What’s Inside The Tin

The following functions are implemented:

  - `mc_media_list`: Return multiple media sources
  - `mc_stats`: Return basic summary stats about total sources, stories,
    feeds, etc processed by Media Cloud
  - `mediacloud_api_key`: Get or set MEDIACLOUD\_API\_KEY value

## Installation

``` r
devtools::install_git("https://git.rud.is/hrbrmstr/mediacloud.git")
# or
devtools::install_git("https://git.sr.ht/~hrbrmstr/mediacloud")
# or
devtools::install_gitlab("hrbrmstr/mediacloud")
# or
devtools::install_bitbucket("hrbrmstr/mediacloud")
# or
devtools::install_github("hrbrmstr/mediacloud")
```

## Usage

``` r
library(mediacloud)

# current version
packageVersion("mediacloud")
## [1] '0.1.0'
```

## mediacloud Metrics

| Lang | \# Files |  (%) | LoC |  (%) | Blank lines |  (%) | \# Lines |  (%) |
| :--- | -------: | ---: | --: | ---: | ----------: | ---: | -------: | ---: |
| R    |        7 | 0.88 | 115 | 0.93 |          33 | 0.65 |      115 | 0.79 |
| Rmd  |        1 | 0.12 |   9 | 0.07 |          18 | 0.35 |       31 | 0.21 |

## Code of Conduct

Please note that this project is released with a [Contributor Code of
Conduct](CONDUCT.md). By participating in this project you agree to
abide by its terms.
